from mindy.graph import basics as bs
from functools import reduce

animal= bs.Graph()
birds= bs.Node(animal, {'class':'птица'})   #класс птиц
animals= bs.Node(animal, {'class':'зверь'})     #класс животных
flying= bs.Node(animal, {'can':'умеет летать'})
birds.Connect(flying)
not_flying= bs.Node(animal, {'cannot':'не умеет летать',})
birds.Connect(not_flying)
parrot= bs.Node(animal, {'genus':'попугай'})       #попугай
flying.Connect(parrot)
ostrich= bs.Node(animal,{'genus': 'страус'})     #страус
not_flying.Connect(ostrich)
grey= bs.Node(animal, {'can': 'серое'})
animals.Connect(grey)
not_grey= bs.Node(animal,{'cannot': 'не серое'})
animals.Connect(not_grey)
wolf= bs.Node(animal, {'genus':'волк'})
grey.Connect(wolf)
firefox= bs.Node(animal,{'genus':'лиса'})
not_grey.Connect(firefox)

def begin():
    print('Загадай животное, а я попробую его отгадать. Для продолжения нажми Enter')
    print('------------------------------------------------------------------------')
    answer=input()

    print('Это птица?')
    answer=(input()).lower()
    if answer=='да':
        current='птица'
        ask(current, answer)

    elif answer=='нет':
        current='зверь'
        answer='да'
        ask(current, answer)

def ask(current_v, answer, current_k='class'):
    if answer=='да':
        current_node= animal.MatchOne({current_k: current_v}).Child({})
    else:
        current_node= animal.MatchOne({current_k: current_v})
    new_unit=''
    new_value=''
    if current_node.Children({}) != []:
        if answer=='да':
            current_v=current_node['can']
            current_k='can'
            print('Это животное %s?'%current_v)
            answer=(input()).lower()
            ask(current_v, answer, current_k)
        elif answer=='нет':
            # в строке ниже сначала в графе находится узел {current_k: current_v}, потом его родителя, потом child c {'cannot': ...},  далле занчение ключа 'cannot' присваивается переменной
            current_node= current_node.Parent({}).Child({'cannot': ' '.join(['не',current_v])})
            current_v=current_node['cannot']
            current_k='cannot'
            current_node= animal.MatchOne({current_k: current_v})
            if current_node.Child({}).Children({}) !=[]:
                answer='да'
                ask(current_v, answer, current_k)
            else:
                current_v=current_node.Child({})['genus']
                current_k='genus'
                #answer='да'
                ask(current_v, answer, current_k)
    else:
        current_v= current_node['genus']
        current_k='genus'
        print('Это %s?'%current_v)
        answer=(input()).lower()
        if answer=='да':
            print('Ура! Я угадал! Хочешь сыграть еще раз?')
            answer=(input()).lower()
            if answer=='да':
                begin()
            elif answer=='нет':
                print('Спасибо за игру, было весело')
        elif answer=='нет':
            print('Я сдаюсь. Кто это?')
            new_unit=(input()).lower()
            print('Чем отличается %s от %s?'%(new_unit, current_v))
            new_value=(input()).lower()
            new_value= delete_words(new_value, new_unit, current_v)
            add_new_branch(new_unit, new_value, current_v)
            print('Хорошо, я запомнил это. Хочешь еще сыграть?')
            answer=(input()).lower()
            if answer=='да':
                begin()
            elif answer=='нет':
                print('Спасибо за игру, было весело')

            
def add_new_branch(new_genus, new_value, old_genus):

    old_node= animal.MatchOne({'genus':old_genus})
    parent= old_node.Parent({})
    parent.Child({'genus': old_genus}).Delete()
    new_node= bs.Node(animal, {'genus': new_genus})
    old_node= bs.Node(animal, {'genus': old_genus})

    a1= bs.Node(animal, {'can': new_value})
    parent.Connect(a1)
    a1.Connect(new_node)

    a2= bs.Node(animal, {'cannot': ''.join(['не ', new_value])})
    parent.Connect(a2)
    a2.Connect(old_node)

def delete_words(sentence, key_word1, key_word2):
    list_keys=['он', 'она', 'оно']
    list_keys= list_keys + [key_word1] + [key_word2]
    words= sentence.split()
    [words.remove(key_word) for key_word in list_keys if key_word in words]
    return ' '.join(words)

begin()