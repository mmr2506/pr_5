# -*- coding: utf-8 -*-

import sys
import sleekxmpp

class SendMsgBot(sleekxmpp.ClientXMPP):

    def __init__(self, jid, password, recipient, message):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)
        self.recipient = recipient
        self.msg = message
        self.add_event_handler("session_start", self.start)

    def start(self, event):
        self.send_presence()
        self.get_roster()
        self.send_message(mto=self.recipient,
                          mbody=self.msg,
                          mtype='chat')
        self.disconnect(wait=True)


def MessageXmpp(jid,to,message,password = 'None'):

    xmpp = SendMsgBot(jid,password, to, message)

    if xmpp.connect(reattempt=False):
        xmpp.process(block=True)
        print("Done")
    else:
        print("Unable to connect.")
