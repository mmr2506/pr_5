# -*- coding: utf-8 -*-
from .neo4j_graph import *
import Stemmer
from operator import itemgetter
from nltk.metrics import distance
import os
import sys
from mindy.graph import basics as bg
import traceback
from mindy.nlp import tools
import xlwt
import locale
from transliterate import translit, get_available_language_codes

import time
import calendar
from datetime import datetime
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
import base64


def group_task(dict_node):
    ls_id_1 = []
    new_ls = []
    ls_task =[]
    for ch in dict_node:
        ls_id_1.append(ch['id'])
    for ch in dict_node:
        if ch['id'] in ls_id_1:
            pr = ch.parents({'type':'задача'})
            if len(pr)==1:
                if pr[0]['id'] not in ls_task and pr[0]['статус']=='выполнено':
                    new_ls.append([pr[0]])
                    ls_task.append(pr[0]['id'])
                    for ch_pr in pr[0].children({'type':'задача'}):
                        if ch_pr['id'] in ls_id_1:
                            new_ls.append(ch_pr)
                            ls_id_1.remove(ch_pr['id'])
                else:
                    for ch_pr in pr[0].children({'type':'задача'}):
                        if ch_pr['id'] in ls_id_1:
                            new_ls.append([ch_pr])
                            ls_id_1.remove(ch_pr['id'])
            else:
                new_ls.append([ch])
    return new_ls

def time_difference(time1, time2):
    '''
    считает разницу в днях между двумя датами представленных в формате timestamp
    '''
    return (datetime.fromtimestamp(float(time1)) - datetime.fromtimestamp(
        float(time2))).total_seconds() / 60 / 60 / 24


def time_format(time1, format):
    '''
    преобразует дату из формата timestamp в указанный формат
    '''
    return datetime.fromtimestamp(float(time1)).strftime(format)


def columns_3_4_5(dict_node):
    '''для отчета по работе за месяц выесняет значение столбцов 3,4,5'''
    ch_ch = dict_node
    columns_3 = 'еще не выполнена'
    columns_4 = 'не назначена'
    columns_5 = '0'
    font_4 = False
    if 'дата_выполнения' not in ch_ch and 'сделать_к' in ch_ch:
        columns_4 = time_format(ch_ch['сделать_к'],
                                "%d %B %Y, %H:%M")
        if time_difference(time.time(), ch_ch['сделать_к']) < 0:
            columns_5 = 'еще не просрочено'
        else:
            columns_5 = round(time_difference(time.time(), ch_ch['сделать_к']),
                              2)
            font_4 = True
    elif 'дата_выполнения' not in ch_ch and 'сделать_к' not in ch_ch:
        columns_3 = 'еще не выполнена'
        columns_4 = 'не назначена'
        columns_5 = '0'
    else:
        columns_3 = time_format(ch_ch['дата_выполнения'], "%d %B %Y, %H:%M")
        if 'сделать_к' in ch_ch:
            columns_4 = time_format(ch_ch['сделать_к'], "%d %B %Y, %H:%M")
            if time_difference(ch_ch['дата_выполнения'], ch_ch['сделать_к']) < 0:
                columns_5 = 'выполнено раньше срока'
            else:
                columns_5 = round(time_difference(ch_ch['дата_выполнения'],
                                                  ch_ch['сделать_к']), 2)
                font_4 = True
    return [columns_3, columns_4, columns_5],font_4



def create_xlm(dict_rez):
    book = xlwt.Workbook('utf8')
    sheet = book.add_sheet('результат')
    font = xlwt.easyxf(
        'font: height 240,name Arial,colour_index black, bold off,italic off;')
    font2 = xlwt.easyxf('font: height 240,name Arial,colour_index black,' + \
                        ' bold off,italic off; pattern: pattern solid,' + \
                        ' fore_colour gray25; border: left medium,top medium,' + \
                        'right medium,bottom medium; alignment: horz ' + \
                        'distributed,vert distributed')
    font4 = xlwt.easyxf('font: height 240,name Arial,colour_index black, ' + \
                        'bold off,italic off; pattern: pattern solid, ' + \
                        'fore_colour red; border: left medium,top medium,' + \
                        'right medium,bottom medium; alignment: horz ' + \
                        'distributed,vert distributed')
    font3 = xlwt.easyxf('font: height 240,name Arial,colour_index black, ' + \
                        'bold off,italic off; border: left thin,top thin,' + \
                        'right thin,bottom thin; alignment: horz justified,' + \
                        'vert center')
    font5 = xlwt.easyxf('font: height 240,name Arial,colour_index black, ' + \
                        'bold on,italic off; border: left thin,top thin,' + \
                        'right thin,bottom thin; alignment: horz justified,' + \
                        'vert center')
    sheet.row(0).height = 600
    sheet.col(0).width = 1500
    sheet.col(1).width = 10000
    sheet.col(2).width = 5000
    sheet.col(3).width = 5000
    sheet.col(4).width = 5000
    sheet.col(5).width = 5000
    sheet.col(6).width = 3000
    sheet.col(7).width = 3000
    sheet.col(8).width = 3000

    i = 0
    sheet.write(0, 0, 'Результаты работы пользователя', font)
    for ch in dict_rez:
        i = i + 1
        sheet.row(i).height = 500
        sheet.write(i, 0, ch, font)
        i = i + 1
        sheet.row(i).height = 750
        sheet.write(i, 0, 'ID', font2)
        sheet.write(i, 1, 'имя', font2)
        sheet.write(i, 2, 'дата создания', font2)
        sheet.write(i, 3, 'дата выполнения', font2)
        sheet.write(i, 4, 'сделать к', font2)
        sheet.write(i, 5, 'просрочено на количество дней', font2)
        sheet.write(i, 6, 'уровень сложности', font2)
        sheet.write(i, 7, 'статус', font2)
        sheet.write(i, 8, 'бюджет', font2)
        dict_ch = dict_rez[ch]
        if 'задачи выполненные пользователем' in ch or 'задачи созданные пользователем' in ch or 'пользователь ответственен за задачи' in ch:
            dict_ch = group_task(dict_rez[ch])
        for ch_ch in dict_ch:
            i = i + 1
            if type(ch_ch) is list:
                ch_ch = ch_ch[0]
                print(ch_ch)
                sheet.write(i, 1, ch_ch['имя'], font5)
            else:
                sheet.write(i, 1, ch_ch['имя'], font3)

            sheet.row(i).height = 1000
            sheet.write(i, 0, ch_ch['номер'], font3)

            locale.setlocale(locale.LC_ALL, ('ru_RU', 'utf-8'))
            sheet.write(i, 2,
                        time_format(ch_ch['создано'], "%d %B %Y, %H:%M"),
                        font3)
            locale.setlocale(locale.LC_ALL, ('ru_RU', 'utf-8'))
            ls_3_4_5,font_4 = columns_3_4_5(ch_ch)
            sheet.write(i, 3, ls_3_4_5[0], font3)
            sheet.write(i, 4, ls_3_4_5[1], font3)
            if font_4:
                sheet.write(i, 5, ls_3_4_5[2], font4)
            else:
                sheet.write(i, 5, ls_3_4_5[2], font3)
            if 'сложность' in ch_ch:
                sheet.write(i, 6, ch_ch['сложность'], font3)
            else:
                sheet.write(i, 6, 'не назначена', font3)

            sheet.write(i, 7, ch_ch['статус'], font3)
            if 'бюджет' in ch_ch:
                sheet.write(i, 8, ch_ch['бюджет'], font3)
            else:
                sheet.write(i, 8, 0, font3)

        i = i + 1
    book.save('result.xls')
    return


def stem(word):
    # получение простемленное формы слова
    result = tools.morph.stem(word)
    return result


def send_file(mail, content, filename):
    msg = MIMEMultipart()
    msg['Subject'] = "Письмо от Минди"
    msg['From'] = "mindy@mindy-labs.com"
    msg['To'] = mail
    part = MIMEBase('application', "csv")
    part.set_payload(str(base64.encodebytes(content.encode()), 'ascii'))
    part.add_header('Content-Transfer-Encoding', 'base64')
    part.add_header('Content-Disposition',
                    'attachment; filename="' + filename + '.csv"')
    msg.attach(part)
    try:
        server = smtplib.SMTP("127.0.0.1")
        o = server.sendmail(msg['From'], msg['To'], msg.as_string())
        print(msg['To'])
        print(o)
        return ({"result": True})
    except:
        traceback.print_exc()
        return ({"result": False})


def send_to(mail, title, _from, content, filename):
    msg = MIMEMultipart()
    msg['Subject'] = title
    msg['From'] = _from
    msg['To'] = mail
    part = MIMEBase('application', "csv")
    part.set_payload(str(base64.encodebytes(content.encode()), 'ascii'))
    part.add_header('Content-Transfer-Encoding', 'base64')
    part.add_header('Content-Disposition',
                    'attachment; filename="' + filename + '.txt"')
    msg.attach(part)
    try:
        server = smtplib.SMTP("localhost")
        o = server.sendmail(msg['From'], msg['To'], msg.as_string())
        print(msg['To'])
        print(o)
        return ({"result": True})
    except:
        traceback.print_exc()
        return ({"result": False})


def send_to_mindy(mail, content, filename):
    msg = MIMEMultipart()
    msg['Subject'] = "Письмо от Минди"
    msg['From'] = "mindy@meanotek.io"
    msg['To'] = mail
    part = MIMEBase('application', "csv")
    part.set_payload(str(base64.encodebytes(content.encode()), 'ascii'))
    part.add_header('Content-Transfer-Encoding', 'base64')
    part.add_header('Content-Disposition',
                    'attachment; filename="' + filename + '.txt"')
    msg.attach(part)
    try:
        server = smtplib.SMTP("localhost")
        o = server.sendmail(msg['From'], msg['To'], msg.as_string())
        print(msg['To'])
        print(o)
        return ({"result": True})
    except:
        traceback.print_exc()
        return ({"result": False})


get_data = tools.get_data


def is_non_zero_file(fpath):
    return os.path.isfile(fpath) and os.path.getsize(fpath) > 0


def jaccard_similarity(m1, m2, stemmer):
    # функция считает похожесть m1 и m2
    # 1. считаем количество совпадающих слов
    # 2. если их нет считаем расстояние - edit_distance
    words1 = (refine(m1.lower().strip()).split())
    words2 = (refine(m2.lower().strip()).split())
    lenght = len(m1)
    acc = 0.0
    for x in words1:
        if x in words2:
            acc += 1.0
    text_acc = 0.0
    if lenght != 0:
        text_acc = acc / float(lenght)
    if text_acc != 0:
        return acc / float(lenght) * 5
    else:
        m1 = (refine(m1.lower().strip()).split())
        m1 = ([stemmer.stemWord(x) for x in m1])
        m2 = (refine(m2.lower().strip()).split())
        m2 = ([stemmer.stemWord(x) for x in m2])
        if len(m1) == 1:
            for x in m2:
                q = distance.edit_distance(m1[0], x)
                if q == 1:
                    m1[0] = x

        total = len(set(m1) | set(m2))
        q = set(m1) & set(m2)
        sim = len(q) / float((total))
        return sim


def edit_distance(name1, name2):
    return distance.edit_distance(name1, name2)


def refine(st):
    st = st.replace(",", " , ")
    st = st.replace("!", " ! ")
    st = st.replace("?", " ? ")
    st = st.replace(":", " : ")
    return st


def check_conformity(graph, node, conforms):
    result = []
    for x in conforms:
        if node["type"] == x[1]:
            new_type = x[0]
            node_new = node
            node_new['type'] = new_type
            res = super(PersistentFuzzyGraph, graph).Match(node_new)
            result.extend(res)
    return result


class PersistentFuzzyGraph(PersistentGraph):

    def __init__(self, graph_connection, graph_segment=None, NodeAddStart=None,
                 NodeAddEnd=None, AddEdgeEnd=None,
                 SetProperty=None, NodeDelete=None, def_segment=None):
        # super(Graph, self).__init__()
        self.nodes = []
        self.edges = {}
        self.run_context = {}
        self.onNodeAddStart = NodeAddStart
        self.onNodeAddEnd = NodeAddEnd
        self.onNodeAddEdgeEnd = AddEdgeEnd
        self.onNodeDelete = NodeDelete
        self.SetProperty = SetProperty
        self.graph_segment = graph_segment
        self.graph_connection = graph_connection
        self.stemmer = Stemmer.Stemmer('russian')
        self.default_segment = def_segment
        # self.nodes.append(Node(self,{"type":"empty"}))

    def SendMeMail(self):
        user = self.run_context["user"]
        if not "@" in user:
            user = user + "@gmail.com"
        # gen_table
        last_result = self.run_context["last_result"]
        fields = []
        fields_string = ""
        lines = []
        for x in last_result[0]:
            if not x.startswith("_"):
                fields.append(x)
                fields_string = fields_string + x + ";"
        for x in last_result:
            line = ""
            for y in fields:
                if y in x:
                    line = line + x[y] + ";"
                else:
                    line = line + "N/A" + ";"
            lines.append(line)
        strx = fields_string + "\n"
        for x in lines:
            strx = strx + x + "\n"
        return send_file(user, strx, "report")

    def SendMeMailXls(self):
        create_xlm(self.run_context["last_result"])
        text = ', '.join(
            ['<br>' + (str(x) + ' ' + str(len(y)) + 'шт.') for (x, y) in
             self.run_context["last_result"].items()])
        name_list = ', '.join(
            [str(x) for (x, y) in self.run_context["last_result"].items()])
        name_node = super(PersistentFuzzyGraph, self).Match(
            {'type': 'пользователь'})
        name_node_name = [i['имя'] for i in name_node]
        for i in name_node_name:
            if i in name_list:
                name = i
        text = 'Результат работы за текущий месяц пользователя' + ' ' + name + ':<br> ' + text
        user = self.run_context["user"]
        mail = super(PersistentFuzzyGraph, self).MatchOne(
            {'type': 'пользователь', 'login': user})
        mail = mail['e_mail']
        msg = MIMEMultipart()
        msg['Subject'] = "Письмо от Минди"
        msg['From'] = "mindy@mindy-labs.com"
        msg['To'] = mail
        filename = 'result'
        part = MIMEBase('application', "xls")
        part.set_payload(
            str(base64.encodebytes(open('result.xls', "rb").read()), 'ascii'))

        part.add_header('Content-Transfer-Encoding', 'base64')

        part.add_header('Content-Disposition',
                        'attachment; filename="' + filename + '.xls"')
        msg.attach(part)
        str_ch = '<!doctype html><html><head><meta charset="UTF-8"></head><body><div border="0" cellpadding="18" cellspacing="0" width="100%" style="border: 10px inset #4CAAD8;"><div valign="top" style="color: #4CAAD8;font-family: Helvetica;font-size: 14px;font-style: italic;font-weight: normal;line-height: 150%;text-align: center;margin-top: 35px;"><span style="color:#ADD8E6;font-size:29px;font-family:times new roman,times,baskerville,georgia,serif">' + text + '</span></div><br><br><span style="color:#4CAAD8;font-size:20px;font-family:georgia; font-style:italic;">Вы получили данное письмо, потому что являетись пользователем системы Минди. С любовью, ваша Минди! </span></div></body></html>'
        s = smtplib.SMTP('127.0.0.1')
        part2 = MIMEText(str_ch, _charset='koi8-r', _subtype='html')
        msg.attach(part2)
        s.connect()
        s.sendmail(msg['From'], msg['To'], msg.as_string())
        s.quit

    def AddNode(self, node):
        if self.onNodeAddStart is not None:
            # запускаем коллбэк из graph_collection
            node = self.onNodeAddStart(self, node, self.run_context)
        result = super(PersistentFuzzyGraph, self).AddNode(node)
        if self.onNodeAddEnd is not None:
            # запускаем коллбэк из graph_collection
            result = self.onNodeAddEnd(self, result, self.run_context)
        return result

    def Delete(self, node):
        result = super(PersistentFuzzyGraph, self).Delete(node)
        if self.onNodeDelete is not None:
            # запускаем коллбэк из graph_collection
            node = self.onNodeDelete(self, node, self.run_context)

    def Match(self, node):
        # функция ищет в графе node
        # print('------------------match-------------------')
        # точное совпадение
        result = super(PersistentFuzzyGraph, self).Match(node)
        # если пользователь распознался англ.буквами
        if 'type' in node:
            if node['type'] == 'пользователь':
                if 'имя' in node:
                    if len(result) == 0:
                        # ----------транслитерация здесь
                        node['имя'] = translit(node['имя'], 'ru')
                        result = super(PersistentFuzzyGraph, self).Match(node)
        # ищет в last_result похожие
        if len(result) == 0:
            if "имя" in node:
                name = node["имя"]
                del node['имя']
                if 'last_result' in self.run_context:
                    if type(self.run_context["last_result"]) == bg.NodeList:
                        outcomes = []
                        last_result = self.run_context["last_result"]
                        res = last_result.Match(node)
                        result = basics.NodeList(self)
                        # считаем для каждого похожесть и добавляем в список
                        for x in res:
                            sim = jaccard_similarity(name, x["имя"],
                                                     self.stemmer)
                            if sim > 0.18:
                                outcomes.append((sim, x))
                        # сортируем по схожести
                        result1 = [x[1] for x in sorted(outcomes, reverse=True,
                                                        key=itemgetter(0))]
                        for x in result1:
                            result.append(x)
                    if len(result) > 0:
                        # возвращаем самый похожий
                        return result
                if not result:
                    # ищем без имени и считаем похожесть
                    result2 = super(PersistentFuzzyGraph, self).Match(node)
                    outcomes = []
                    for x in result2:
                        if "имя" in x:
                            if 'type' in node:
                                if (node['type'] == 'пользователь'):
                                    # пользователей обрабатываем отдельно, т.к. у них отрицательный sim
                                    sim = edit_distance(name, x["имя"])
                                    sim = sim * (-1)
                                else:
                                    sim = jaccard_similarity(name, x["имя"],
                                                             self.stemmer)
                                    if 'создано' in x:
                                        # учитываем также дату создания
                                        if sim != 0.0:
                                            sim = float(sim) - (
                                                    (float(time.time()) - float(
                                                        x[
                                                            'создано'])) * 0.00000000005)
                                outcomes.append((sim, x))
                    # сортируем результат
                    outcomes = [x for x in outcomes if x[0] > 0.2 or x[0] < 0.0]
                    result1 = [x[1] for x in sorted(outcomes, reverse=True,
                                                    key=itemgetter(0))]
                    result = basics.NodeList(self)
                    for x in result1:
                        result.append(x)
        return result

    def if_statement(self, statemnt, user):
        node = super(PersistentFuzzyGraph, self).Match(
            {'type': 'пользователь', 'login': user})
        if 'редактирование' in node[0]:
            if node[0]['редактирование'] == 'false':
                if 'SetRemove' in statemnt or 'MoveTo' in statemnt or 'InOrSet' in statemnt or 'RelatedBy' in statemnt:
                    return False
                if 'Set' in statemnt:
                    if 'имя' in statemnt or 'описание' in statemnt or 'исполнитель' in statemnt or 'сделать_к' in statemnt or 'созданно' in statemnt or 'выполнено' in statemnt or 'начало_выполнения' in statemnt or 'срок_выполнения' in statemnt:
                        return False
        if 'удаление' in node[0]:
            if node[0]['удаление'] == 'false':
                if 'Delete' in statemnt:
                    return False
        if 'создание' in node[0]:
            if node[0]['создание'] == 'false':
                if 'AddChain' in statemnt and not 'коментарий' in statemnt:
                    return False
                if 'Node' in statemnt and not 'коментарий' in statemnt and not 'проблема' in statemnt:
                    return False
        return True

    def run(self, statement, user, last_result=None):
        print('statement   ' + statement)
        current_user = user
        self.run_context["user"] = current_user
        now = time.time()
        print("-----------------IN GRAPH ENTER -------------")
        print("-----------------IN GRAPH -------------")
        if last_result is not None:
            last_result_list = last_result
            last_result = last_result_list[len(last_result_list) - 1]
            self.run_context["last_result"] = last_result
        else:
            last_res = []
            last_result = bg.NodeList(last_res)
            last_result.parent_graph = self
            last_result_list = bg.NodeList(last_res)
            last_result_list.parent_graph = self
        statement = statement.strip()
        print("-----------------IN LAST -------------")
        if ';' in statement:
            new_statement = statement.split(';')
            for x in new_statement:
                print('next')
                p = self.run(x.strip(), user, last_result=last_result_list)
            return p
        prefix = 'self.'
        statement = statement.replace("Node(", "bg.Node(" + 'self,')
        statement = statement.replace("(Match", "(" + prefix + "Match")
        statement = statement.replace("(MatchOne", "(" + prefix + "MatchOne")
        statement = statement.replace(",MatchOne", "," + prefix + "MatchOne")
        statement = statement.replace(" MatchOne", " " + prefix + "MatchOne")
        print('statement---------------' + statement)
        if_st_b = self.if_statement(statement, current_user)
        if if_st_b:
            try:
                if statement.startswith("bg.Node"):
                    result = eval(statement)
                    pass
                else:
                    if not statement.startswith("last_result"):
#                        print('??????????', statement)
                        result = eval('self.' + statement)
                    else:
                        result = eval(statement)
            except Exception as err:
                print("unexecutable resut: " + statement)
                print('self.')
                print('555555555555')
                print(sys.exc_info()[1])
                print(traceback.format_exc())
                return (False)
            self.run_context = {}
            # print('<><><><><><><>',result)
            return result
        else:
            return False

    def AddEdge(self, node1, node2):
        result = super(PersistentFuzzyGraph, self).AddEdge(node1, node2)
        if self.onNodeAddEdgeEnd is not None:
            # запускаем коллбэк из graph_collection
            result = self.onNodeAddEdgeEnd(self, node1, node2)

    def UpdateNodeProperty(self, node, property_name, new_property_value):
        if self.SetProperty == None:
            # запускаем коллбэк из graph_collection
            result = super(PersistentFuzzyGraph, self).UpdateNodeProperty(node,
                                                                          property_name,
                                                                          new_property_value)
        else:
            result = super(PersistentFuzzyGraph, self).UpdateNodeProperty(node,
                                                                          property_name,
                                                                          new_property_value)
            self.SetProperty(self, node, property_name, new_property_value,
                             self.run_context)

    def ChooseTask(self):
        # функция назначения задачи пользователю
        user = self.run_context["user"]
        # ищем в базе невыполненные задачи и проблемы пользователя
        tasks = super(PersistentFuzzyGraph, self).MatchOne(
            {'type': 'пользователь', 'login': user}).children(
            {'type': 'задача', 'статус': 'не выполнен'})
        problems = super(PersistentFuzzyGraph, self).MatchOne(
            {'type': 'пользователь', 'login': user}).children(
            {'type': 'проблема', 'статус': 'не выполнен'})
        tasks1 = []
        tasks2 = []
        tasks1 = bg.NodeList(tasks1)
        tasks1.parent_graph = self
        tasks2 = bg.NodeList(tasks2)
        tasks2.parent_graph = self
        for x in tasks:
            if '_приоритет' in x:
                tasks1.append(x)
            else:
                tasks2.append(x)
        # найденные задачи и проблемы сортируем по приоритетам
        tasks1 = tasks1.Sorting('_приоритет')
        tasks2 = tasks2.Sorting('приоритет')
        tasks = tasks1 + tasks2
        prob1 = []
        prob2 = []
        prob1 = bg.NodeList(prob1)
        prob1.parent_graph = self
        prob2 = bg.NodeList(prob2)
        prob2.parent_graph = self
        for x in problems:
            if '_приоритет' in x:
                prob1.append(x)
            else:
                prob2.append(x)
        prob1 = prob1.Sorting('_приоритет')
        prob2 = prob2.Sorting('приоритет')
        problems = prob1
        for x in prob2:
            problems.append(x)
        all_tasks = problems
        for x in tasks:
            all_tasks.append(x)
        if type(all_tasks) is list:
            all_tasks = bg.NodeList(all_tasks)
            all_tasks.parent_graph = self
        # если для пользователя ничего не нашлось, то проверяем не закреплен ли за ним раздел и ищем там
        # алгоритм аналогичный тому, что выше
        if len(all_tasks) == 0:
            user = super(PersistentFuzzyGraph, self).MatchOne(
                {'type': 'пользователь', 'login': user})['имя']
            tasks = super(PersistentFuzzyGraph, self).MatchOne(
                {'type': 'раздел', 'исполнитель': user}).children(
                {'type': 'задача', 'статус': 'не выполнен'})
            problems = super(PersistentFuzzyGraph, self).MatchOne(
                {'type': 'раздел', 'исполнитель': user}).children(
                {'type': 'проблема', 'статус': 'не выполнен'})
            tasks1 = []
            tasks2 = []
            tasks1 = bg.NodeList(tasks1)
            tasks1.parent_graph = self
            tasks2 = bg.NodeList(tasks2)
            tasks2.parent_graph = self
            for x in tasks:
                if '_приоритет' in x:
                    tasks1.append(x)
                else:
                    tasks2.append(x)
            tasks1 = tasks1.Sorting('_приоритет')
            tasks2 = tasks2.Sorting('приоритет')
            tasks = tasks1 + tasks2
            prob1 = []
            prob2 = []
            prob1 = bg.NodeList(prob1)
            prob1.parent_graph = self
            prob2 = bg.NodeList(prob2)
            prob2.parent_graph = self
            for x in problems:
                if '_приоритет' in x:
                    prob1.append(x)
                else:
                    prob2.append(x)
            prob1 = prob1.Sorting('_приоритет')
            prob2 = prob2.Sorting('приоритет')
            problems = prob1
            for x in prob2:
                problems.append(x)
            all_tasks = problems
            for x in tasks:
                all_tasks.append(x)
            if type(all_tasks) is list:
                all_tasks = bg.NodeList(all_tasks)
                all_tasks.parent_graph = self
        # убираем из результата отложенные задачи
        for i in range(len(all_tasks)):
            if 'начало_выполнения' in all_tasks[i]:
                data = all_tasks[i]['начало_выполнения']
                now = time.time()
                if float(data) > float(now):
                    all_tasks[i] = {}
        all_tasks = [x for x in all_tasks if x != {}]
        # возвращаем самый приоритетный вариант
        if len(all_tasks) == 0:
            all_tasks = bg.NodeList(all_tasks)
            all_tasks.parent_graph = self
            return all_tasks
        else:
            return all_tasks[0]

    def Copy(self, child, new_name, new_parent=None, history=[]):
        # функция создания копии указанного раздела, вместе с его потомками
        # функция рекурсивная
        # алгоритм обхода дерева
        print("COPYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY")
        node = {}
        aval = False
        for x in history:
            if child['id'] == x[0]['id']:
                aval = True
        if not aval:
            for x in child:
                node[x] = child[x]
            if node["type"] == 'раздел':
                node['имя'] = new_name
            node = self.AddNode(node)
            history.append((child, node))
        else:
            for x in history:
                if x[0]['id'] == child['id']:
                    node = x[1]
        if new_parent != None:
            node.AddParent(new_parent)
        people = self.MatchOne(child).children({})
        print("||||||||||||||||||||||||||||||||||||||||||||")
        if people:
            for x in people:
                self.Copy(x, new_name, node, history)
        return new_parent

    def DependentChain(self, ListNodes):
        # создает цепочку зависимых задач
        # по порядку создает зависимость одной задачи от следующей
        # создается узел зависимости и добавляют связи к главной и зависимой задача соответственно
        i = len(ListNodes) - 1
        while (i > 0):
            ListNodes[i].RelatedBy('зависимость', ListNodes[i - 1]).Set(
                'статус', 'есть зависимость').AddParent(
                self.AddNode(
                    {'type': 'зависимость', 'имя': 'зависит от'}).AddParent(
                    ListNodes[i - 1]))
            i = i - 1
        return ListNodes

    def CleverDelete(self, node):
        # умное удаление
        # проверяем если есть потомки у объекта, то не удаляем его
        DEL = {'result': False}
        connections = node.children()
        print('>>>>>>>>>>>>>>>>>>>>>>', node)
        if node['type'] == 'пользователь':
            self.DeleteNode(node)
            DEL = {'result': True}
        else:
            if not connections:
                self.DeleteNode(node)
                DEL = {'result': True}
                if self.onNodeDelete is not None:
                    self.onNodeDelete(self, node, self.run_context)
            else:
                remin = True
                for x in connections:
                    if 'type' in x:
                        if x['type'] != 'уведомление':
                            remin = False
                if remin:
                    self.DeleteNode(node)
                    DEL = {'result': True}
                    if self.onNodeDelete is not None:
                        self.onNodeDelete(self, node, self.run_context)
                else:
                    print(
                        '+++++++++++++++++++++++++++++++++++++++++++++++++++++')
                    print(
                        'I can not perform the deletion. The node has children')
                    for x in connections:
                        # выводим потомков
                        print(x)
                        print('___________')
        return DEL

    def AllChildren(self, node, result=None):
        # получает всех потомков у node
        # функция рекурсивная - обход дерева
        if result == None:
            result = bg.NodeList(result)
            result.parent_graph = self
            people = node.children()
            for x in people:
                result.append(x)
        else:
            people = node.children()
            for x in people:
                result.append(x)

        if people:
            for x in people:
                self.AllChildren(x, result)
        return result

    def UpdateChain(self, node):
        # обновление цепочки
        # получаем всех потомков у node
        # ищем среди них узлы зависимости и в соответствии с ними меняет статусы задачам
        all_nodes = self.AllChildren(node)
        dependencies = []
        dependencies = bg.NodeList(dependencies)
        dependencies.parent_graph = self
        for x in all_nodes:
            if x['type'] == 'зависимость':
                res = x.children({'type': 'задача'})
                for y in res:
                    dependencies.append(y)
        for x in dependencies:
            x.Set('статус', 'есть зависимость')
        return node

    def WhatShouldIDo(self, node):
        # показывает пользователю список назначенных ему задач и проблем в соответствии с их приоритетом
        li1 = node.children({'статус': 'в работе'}).Sorting(
            'сделать_к').Sorting('приоритет')
        li2 = node.children(
            {'type': 'проблема', 'статус': 'не выполнен'}).Sorting(
            'сделать_к').Sorting('приоритет')
        li3 = node.children(
            {'type': 'задача', 'статус': 'не выполнен'}).Sorting(
            'сделать_к').Sorting('приоритет')
        li4 = node.children({'статус': 'ждет планирования'}).Sorting(
            'сделать_к').Sorting('приоритет')
        li5 = node.children({'статус': 'отложено'}).Sorting(
            'сделать_к').Sorting('приоритет')
        result = []
        result = bg.NodeList(result)
        result = li1 + li2 + li3 + li4 + li5
        new_result = []
        new_result = bg.NodeList(new_result)
        new_result.parent_graph = self
        # убираем из результата отложенные задачи
        for x in result:
            if 'начало_выполнения' in x:
                data = x['начало_выполнения']
                now = time.time()
                if float(data) > float(now):
                    print("ne nado")
                else:
                    new_result.append(x)
            else:
                new_result.append(x)

        return new_result

    def WhatWillIDo(self, node):
        result2 = node.children({'type': 'раздел'})
        result3 = []
        if len(result2) > 0:
            for ch in result2:
                result4 = result2[0].children()
                for chh in result4:
                    if not 'исполнитель' in chh and chh[
                        'статус'] == 'не выполнен':
                        result3.append(chh)
        new_result = []
        new_result = bg.NodeList(new_result)
        new_result.parent_graph = self
        for x in result3:
            if 'начало_выполнения' in x:
                data = x['начало_выполнения']
                now = time.time()
                if float(data) > float(now):
                    new_result.append(x)
                else:
                    print("ne nado")
            else:
                new_result.append(x)
        return new_result

    def GetDependent(self, node):
        # получает все зависимые от node задач
        # получаем всех потомков node
        all_nodes = self.AllChildren(node)
        dependencies = []
        dependencies = bg.NodeList(dependencies)
        dependencies.parent_graph = self
        for x in all_nodes:
            # если среди потомком есть узлы зависимость, то
            # добавляем зависимые объекты в результат
            if x['type'] == 'зависимость':
                res = x.children({'type': 'задача'})
                for y in res:
                    dependencies.append(y)
        return dependencies

    def GetChain(self, node):
        # получает цепочку зависимых задач, начиная с node
        result = []
        result = bg.NodeList(result)
        result.parent_graph = self
        if 'статус' in node:
            # если node главная задача, то просто получаем всех ее потомков и выбираем с зависимостями
            if node['статус'] != 'есть зависимость':
                result.append(node)
                result.extend(self.GetDependent(node))
                return result
            else:
                # если node от чего-то зависит
                # получаем главную от нее задачу и т.д. пока не дойдем до самой главной
                status = node['статус']
                while status == 'есть зависимость':
                    par = node.parent({'type': 'зависимость'}).parent(
                        {'type': 'задача'})
                    print('upper node')
                    print(par)
                    if par:
                        result.append(par)
                    status = par['статус']
                result2 = self.GetDependent(node)
                result.reverse()
                result.append(node)
                result.extend(result2)
                return result

    def Today(self, list_tasks):
        # из списка list_tasks выбирает те, которые
        # выполнены в течении суток
        day = 43400.0
        result = []
        result = bg.NodeList(result)
        result.parent_graph = self
        now = time.time()
        print('now')
        print(now)
        for x in list_tasks:
            if 'дата_выполнения' in x:
                print('------------------------')
                print(x)
                raz = now - float(x['дата_выполнения'])
                print(raz)
                if (float(x['дата_выполнения']) < now) and (raz < day):
                    print(raz)
                    print('yeeees-------------------')
                    result.append(x)
        return result

    def Yesterday(self, list_tasks):
        # из списка list_tasks выбирает те, которые
        # выполнены в вчера
        today = 28800.0
        yesterday = 108000.0
        result = []
        result = bg.NodeList(result)
        result.parent_graph = self
        now = time.time()
        print('now')
        print(now)
        for x in list_tasks:
            if 'дата_выполнения' in x:
                print('------------------------')
                print(x)
                raz = now - float(x['дата_выполнения'])
                print(raz)
                if (float(x['дата_выполнения']) < now) and (raz > today) and (
                        raz < yesterday):
                    print(raz)
                    print('yeeees-------------------')
                    result.append(x)
        return result

    def Week(self, list_tasks):
        # из списка list_tasks выбирает те, которые
        # выполнены в течение недели
        week = 604800.0
        result = []
        result = bg.NodeList(result)
        result.parent_graph = self
        now = time.time()
        for x in list_tasks:
            if 'дата_выполнения' in x:
                print('------------------------')
                print(x)
                raz = now - float(x['дата_выполнения'])
                print(raz)
                if (float(x['дата_выполнения']) < now) and (raz < week):
                    print(raz)
                    print('yeeees-------------------')
                    result.append(x)
        return result

    def MainTask(self, node):
        return bg.MainTask(self, node)

    def Month(self, list_tasks, responsible=0):
        # из списка list_tasks выбирает те, которые
        # выполнены в течение 30 дней
        month = 2678400.0
        result = []
        result = bg.NodeList(result)
        result.parent_graph = self
        now = time.time()
        for x in list_tasks:
            if responsible != 0:
                if 'дата_выполнения' in x:
                    raz = now - float(x['создано'])
                    if (float(x['создано']) < now) and (raz < month):
                        result.append(x)
            else:
                if 'дата_выполнения' in x:
                    raz = now - float(x['дата_выполнения'])
                    if (float(x['дата_выполнения']) < now) and (raz < month):
                        result.append(x)
        return result

    def Overdue(self, list_tasks):
        # из списка list_tasks выбирает те, у которых
        # нарушен дедлайн
        result = []
        result = bg.NodeList(result)
        result.parent_graph = self
        now = time.time()
        for x in list_tasks:
            if 'сделать_к' in x:
                if (float(x['сделать_к']) < now):
                    if 'дата_выполнения' in x:
                        if float(x['сделать_к']) < float(x['дата_выполнения']):
                            result.append(x)
                    else:
                        result.append(x)
        return result

    def cur_month(self, list_tasks, responsible=0):
        # из списка list_tasks выбирает те, которые
        # выполнены в течение календарного месяца
        now = time.time()
        date = datetime.now()
        date_tuple = date.timetuple()
        now_year = date_tuple[0]
        now_month = date_tuple[1]
        end_day = calendar.monthrange(now_year, now_month)
        end_day = end_day[1]
        start_data = '1/' + str(now_month) + '/' + str(now_year)
        end_data = str(end_day) + '/' + str(now_month) + '/' + str(now_year)
        start_data_number = time.mktime(
            datetime.strptime(start_data, "%d/%m/%Y").timetuple())
        end_data_number = time.mktime(
            datetime.strptime(end_data, "%d/%m/%Y").timetuple())
        result = []
        result = bg.NodeList(result)
        result.parent_graph = self
        for x in list_tasks:
            if responsible != 0:
                if 'создано' in x:
                    if (float(x['создано']) < end_data_number) and (
                            float(x['создано']) > start_data_number):
                        result.append(x)
            else:
                if 'дата_выполнения' in x:
                    if (float(x['дата_выполнения']) < end_data_number) and (
                            float(x['дата_выполнения']) > start_data_number):
                        result.append(x)
        return result

    def last_month(self, list_tasks, responsible=0):
        # из списка list_tasks выбирает те, которые
        # выполнены в течение прошлого календарного месяца
        now = time.time()
        date = datetime.now()
        date_tuple = date.timetuple()
        now_year = date_tuple[0]
        now_month = date_tuple[1]
        if float(now_month) == 1.0:
            now_month = 12
        else:
            now_month = int(float(now_month) - 1.0)
        end_day = calendar.monthrange(now_year, now_month)
        end_day = end_day[1]
        start_data = '1/' + str(now_month) + '/' + str(now_year)
        start_data_number = time.mktime(
            datetime.strptime(start_data, "%d/%m/%Y").timetuple())
        end_data = str(end_day) + '/' + str(now_month) + '/' + str(now_year)
        end_data_number = time.mktime(
            datetime.strptime(end_data, "%d/%m/%Y").timetuple())
        result = []
        result = bg.NodeList(result)
        result.parent_graph = self
        for x in list_tasks:
            if responsible != 0:
                if 'создано' in x:
                    if (float(x['создано']) < end_data_number) and (
                            float(x['создано']) > start_data_number):
                        result.append(x)
            else:
                if 'дата_выполнения' in x:
                    if (float(x['дата_выполнения']) < end_data_number) and (
                            float(x['дата_выполнения']) > start_data_number):
                        result.append(x)
        return result

    def result_of_work(self, user, name=2):
        result = self.Match({'type': 'пользователь', "имя": user})
        if len(result) > 1:
            outcomes = []
            for x in result:
                # пользователей обрабатываем отдельно, т.к. у них отрицательный sim
                sim = edit_distance(user, x["имя"])
                sim = sim * (-1)
                outcomes.append((sim, x))
            outcomes = [x for x in outcomes if x[0] > 0.2 or x[0] < 0.0]
            result1 = [x[1] for x in
                       sorted(outcomes, reverse=True, key=itemgetter(0))]
            result = [result1[0]]
        node_task = super(PersistentFuzzyGraph, self).Match({'type': 'задача'})
        node_priblem = super(PersistentFuzzyGraph, self).Match(
            {'type': 'проблема'})
        node_responsible = super(PersistentFuzzyGraph, self).Match(
            {'ответственный': result[-1]['имя'],'type':'задача'})
        if name == 0:  # прощлый месяц
            node_task_m = self.last_month(node_task)
            node_priblem_m = self.last_month(node_priblem)
            node_responsible_m = self.last_month(node_responsible, 1)
        elif name == 1:  # 30 дней
            node_task_m = self.Month(node_task)
            node_priblem_m = self.Month(node_priblem)
            node_responsible_m = self.Month(node_responsible, 1)
        else:  # текущий месяц
            node_task_m = self.cur_month(node_task)
            node_priblem_m = self.cur_month(node_priblem)
            node_responsible_m = self.cur_month(node_responsible, 1)
        created = []
        executed = []
        he_created = []
        created_by_him = []
        for i in node_task_m:
            if 'создатель' in i and i['создатель'] == result[-1]['имя']:
                created.append(i)
            if 'исполнитель' in i and i['исполнитель'] == result[-1]['имя'] and \
                    i['статус'] == 'выполнен':
                executed.append(i)
        for i in node_priblem_m:
            if 'создатель' in i and i['создатель'] == result[-1]['имя']:
                he_created.append(i)
            if 'исполнитель' in i and i['исполнитель'] == result[-1]['имя']:
                created_by_him.append(i)
        new_result = {
            'задачи созданные пользователем ' + result[-1]['имя']: created,
            'задачи выполненные пользователем ' + result[-1]['имя']: executed,
            'проблемы созданные пользователем ' + result[-1]['имя']: he_created,
            'проблемы созданные для пользователя ' + result[-1][
                'имя']: created_by_him,
            'пользователь ответственен за задачи ': node_responsible_m}
        return new_result

    def tasks_for_today(self):
        user = self.run_context["user"]
        # ищем в базе невыполненные задачи и проблемы пользователя
        tasks = super(PersistentFuzzyGraph, self).Match(
            {'type': 'задача', 'статус': 'не выполнен'})
        problems = super(PersistentFuzzyGraph, self).Match(
            {'type': 'проблема', 'статус': 'не выполнен'})
        tasks1 = []
        tasks2 = []
        tasks1 = bg.NodeList(tasks1)
        tasks1.parent_graph = self
        tasks2 = bg.NodeList(tasks2)
        tasks2.parent_graph = self
        for x in tasks:
            if '_приоритет' in x:
                tasks1.append(x)
            else:
                tasks2.append(x)
        # найденные задачи и проблемы сортируем по приоритетам
        tasks1 = tasks1.Sorting('_приоритет')
        tasks2 = tasks2.Sorting('приоритет')
        tasks = tasks1 + tasks2
        prob1 = []
        prob2 = []
        prob1 = bg.NodeList(prob1)
        prob1.parent_graph = self
        prob2 = bg.NodeList(prob2)
        prob2.parent_graph = self
        for x in problems:
            if '_приоритет' in x:
                prob1.append(x)
            else:
                prob2.append(x)
        prob1 = prob1.Sorting('_приоритет')
        prob2 = prob2.Sorting('приоритет')
        problems = prob1
        for x in prob2:
            problems.append(x)
        all_tasks = problems
        for x in tasks:
            all_tasks.append(x)
        if type(all_tasks) is list:
            all_tasks = bg.NodeList(all_tasks)
            all_tasks.parent_graph = self
        # убираем из результата отложенные задачи
        for i in range(len(all_tasks)):
            if 'начало_выполнения' in all_tasks[i]:
                data = all_tasks[i]['начало_выполнения']
                if datetime.fromtimestamp(
                        float(data)).date() != datetime.fromtimestamp(
                    float(time.time())).date():
                    all_tasks[i] = {}
            elif 'сделать_к' in all_tasks[i]:
                data = all_tasks[i]['сделать_к']
                now = time.time()
                if datetime.fromtimestamp(
                        float(data)).date() != datetime.fromtimestamp(
                    float(time.time())).date():
                    all_tasks[i] = {}
            else:
                all_tasks[i] = {}
        all_tasks = [x for x in all_tasks if x != {}]
        return all_tasks

    def cread_tasks_in_section(self, node):
        print('>>>>>HERE<<<<<<')
        try:
            result = self.run_context["last_result"]
        except:
            return super(PersistentFuzzyGraph, self).MatchOne(
                {'type': "раздел", "имя": "рабочий стол"}).Add(node)
        list_section = []
        section = {}
        new_result = False
        print("TYPE = ", type(result))
        if type(result) != bg.NodeList and 'type' not in result:
            new_result = result.Add(node)
        elif 'type' in result and (
                result['type'] == 'задача' or result['type'] == 'проблема'):
            n = super(PersistentFuzzyGraph, self).Match(result)
            if len(n) != 0:
                k = result.parents({'type': 'раздел'})
                if type(k) == bg.NodeList:
                    new_result = k[-1].Add(node)
                elif type(k) == bg.Node:
                    new_result = k.Add(node)
        elif 'type' in result and result['type'] == 'раздел':
            new_result = result.Add(node)
        else:
            for ch in result:
                n = super(PersistentFuzzyGraph, self).Match(ch)
                if len(n) != 0:
                    k = ch.parents({'type': 'раздел'})
                    if len(k) == 1 and k[0]['имя'] not in list_section:
                        list_section.append(k[0]['имя'])
                        section = k[0]
            print('LIST SECTION =', list_section)
            print('SECTION = ', section)
            if len(list_section) == 1:
                new_result = section.Add(node)
            else:
                new_result = super(PersistentFuzzyGraph, self).MatchOne(
                    {'type': "раздел", "имя": "рабочий стол"}).Add(
                    node)
        return new_result
