# -*- coding: utf-8 -*-
from . import basics


def jaccard_similarity(m1, m2):
    #  print (m1,m2)
    m1 = (refine(m1.lower()).split())
    m1 = set([stemmer.stemWord(x) for x in m1])

    m2 = (refine(m2.lower()).split())
    m2 = set([stemmer.stemWord(x) for x in m2])
    total = len(m1 | m2)
    q = m1 & m2
    sim = len(q) / float((total))
    return sim


class PersistentGraph():

    def __init__(self, graph_connection, graph_segment=None):
        self.nodes = []
        self.edges = {}
        self.graph_segment = graph_segment
        self.graph_connection = graph_connection

    def __format_args(self, dic):
        string = ""

        for x in dic:
            prop = x

            value = dic[x]

            if prop != "type" and prop != 'id':
                if str(value).isdigit():
                    string = string + "" + prop + ":'" + str(value) + "'" + ','
                else:
                    string = string + "" + prop + ":'" + str(value) + "'" + ','

        string = string[:-1]
        return string

    def __dict2query(self, node_name, dic):
        if "type" in dic:
            node_type = dic["type"]

            return node_name + ":" + node_type + "{" + self.__format_args(dic) + '}'
        else:
            return node_name + "{" + self.__format_args(dic) + '}'

    def __dict2where(self, dic):
        string = ""
        for x in dic:
            prop = x
            value = dic[x]
            if prop != "type":
                string = string + "'" + prop + "= {" + str(value) + "}'" + ','

    def AddNode(self, node):
        global k

        @staticmethod
        def create_person_node(tx, name):
            tx.run(name)

        # session = self.graph_connection.session()
        node_type = node["type"]
        if self.graph_segment is not None:
            if type(node) is dict:
                node["_segment"] = self.graph_segment
            else:
                node.dict["_segment"] = self.graph_segment

        strcon = "CREATE (node:" + node_type + " {" + self.__format_args(node).replace('"', '\"').replace("'",
                                                                                                          "\'") + '}) RETURN node'
        print(strcon)
        # result = session.run(strcon)
        # node = self.__neoj2graph_query2nodes(result)[0]

        with self.graph_connection.session() as session:
            with session.begin_transaction() as tx:
                result = tx.run(strcon)

        node = self.__neoj2graph_query2nodes(result)[0]
        # session.close()

        return node

    def AddEdge(self, node1, node2):
        # session = self.graph_connection.session()
        if 'id' in node1 and 'id' in node2:
            query = "MATCH  (node1),(node2) WHERE  id(node1) = " + node1['id'] + " and id(node2)=" + str(
                node2['id']) + " CREATE UNIQUE (node1)-[conn:CONNECTS]->(node2) return id(conn)"
            print(query)
            # session.run(query)
            # session.closed()
            with self.graph_connection.session() as session:
                with session.begin_transaction() as tx:
                    result = tx.run(query)
                    try:
                        return result.values()[0][0]
                    except:
                        return

    def AddEdge2(self, node1, node2, ls_ansvers):
        # session = self.graph_connection.session()
        if 'id' in node1 and 'id' in node2:
            print(ls_ansvers)
            query = "MATCH  (node1),(node2) WHERE  id(node1) = " + node1['id'] + " and id(node2)=" + str(
                node2['id']) + " CREATE UNIQUE (node1)-[conn:" + '|'.join(ls_ansvers) + "]->(node2) return id(conn)"
            print(query)
            # session.run(query)
            # session.closed()
            with self.graph_connection.session() as session:
                with session.begin_transaction() as tx:
                    result = tx.run(query)
                    print(result.value)
                    return result.values()[0][0]

    def __neoj2graph_query2nodes(self, result):
        rlist = []
        u = 0
        for record in result:
            dic = {}
            print(record)
            for x in record["node"]:
                    dic[x] = record["node"][x]
            for x in record["node"].labels:
                dic[x] = x
            
            print(dic)
            dic["node"] = record["node"]
            print("----------------------")    
            rlist.append(dic)
        rlist2 = basics.NodeList(self)
        for d in rlist:
            rlist2.append(self.__neoj2graph_node(d))
        return rlist2

    def __neoj2graph_node(self, neoj, key="node"):
        node = neoj[key]
        ids = node.id
        node_type = list(node.labels)[0]
        other_props = node
        new_dict = {}
        new_dict["id"] = ids
        new_dict["type"] = node_type
        for x in other_props:
            new_dict[x] = other_props[x]

        return basics.Node(self, new_dict, makenew=False)

    def __neoj2graph_query2nodes2(self, result):
        rlist = []
        u = 0
        for  record in result:
            dic = {}
            list_small = []
            for x in record:
                list_small.append({x: record[x]})

            rlist.append(list_small)
        rl2 = []
        for rl in rlist:
            rlist2 = basics.NodeList(self)
            for d in rl:
                # ~ print(d)
                key = list(d)[0]
                # ~ print(key)
                rlist2.append(self.__neoj2graph_node(d, key))
            rl2.append(rlist2)
        return rl2

    def Match(self, node):
        if self.graph_segment is not None:
            if type(node) is dict:
                node["_segment"] = self.graph_segment
            else:
                node.dict["_segment"] = self.graph_segment

        node1 = self.__dict2query("node", node)
        if "id" in node:
            query = "MATCH (node) WHERE id(node)=" + str(node['id']) + " RETURN node"
            # print query
        else:
            query = "MATCH (" + node1 + ") RETURN node"
        print(query)
        session = self.graph_connection.session()
        result = session.run(query)
        session.closed()
        rlist2 = self.__neoj2graph_query2nodes(result)
        if len(rlist2) == 0:
            try:
                if self.default_segment is not None:
                    if type(node) is dict:
                        node["_segment"] = self.default_segment
                    else:
                        node.dict["_segment"] = self.default_segment
                    node1 = self.__dict2query("node", node)
                    query = "MATCH (" + node1 + ") RETURN node"
                    print('22222', query)
                    session = self.graph_connection.session()
                    result = session.run(query)
                    session.closed()
                    rlist2 = self.__neoj2graph_query2nodes(result)
            except:
                pass
        return rlist2

    def NotExists(self, params1, params2):
        session = self.graph_connection.session()
        query = "MATCH (node:" + params1 + ") where not exists(node." + params2 + ")  RETURN node"
        # print(query)
        result = session.run(query)
        session.closed()
        rlist2 = self.__neoj2graph_query2nodes(result)
        return rlist2

    def NotExistsList(self, list_node, params2):
        session = self.graph_connection.session()
        result_list = {}
        rlist2 = []
        for ch in list_node:
            query = "MATCH (node) where ID(node) =" + str(
                ch['id']) + " and not exists(node." + params2 + ")  RETURN node"
            result = session.run(query)
            session.closed()
            k = self.__neoj2graph_query2nodes(result)
            if len(k) > 0:
                rlist2.append(k[0])
        return rlist2

    def MatchOne(self, node):
        return self.Match(node).First()

    def Match1Child(self, node_parent, node_children):
        node = self.__dict2query("node", node_parent)
        node_ch = self.__dict2query("node_children", node_children)
        query = "MATCH (" + node + ")-->(" + node_ch + ") RETURN node";
        # print (query)
        session = self.graph_connection.session()
        result = session.run(query)
        session.closed()
        rlist2 = self.__neoj2graph_query2nodes(result)
        return rlist2

    def Match1Child2(self, node_parent, node_children):
        node = self.__dict2query("node", node_parent)
        node_ch = self.__dict2query("node_children", node_children)
        query = "MATCH (" + node + ")-->(" + node_ch + ") RETURN node_children";
        print(query)
        session = self.graph_connection.session()
        result = session.run(query)
        session.closed()
        rlist2 = self.__neoj2graph_query2nodes2(result)
        return rlist2

    def Match2Child(self, node_parent, node_children1, node_children2):
        node = self.__dict2query("node", node_parent)
        node_ch1 = self.__dict2query("node_children1", node_children1)
        node_ch2 = self.__dict2query("node_children2", node_children2)

        query = "MATCH (" + node_ch2 + ")<--(" + node + ")-->(" + node_ch1 + ") RETURN node";
        # print(query)
        session = self.graph_connection.session()
        result = session.run(query)
        session.closed()
        rlist2 = self.__neoj2graph_query2nodes(result)
        return rlist2

    def Match2Chaild1Child(self, node_parent, node_children11, node_children12, node_children21):
        """
            ch11<--node-->ch12-->ch21
            return node
        """
        node = self.__dict2query("node", node_parent)
        node_ch11 = self.__dict2query("node_children11", node_children11)
        node_ch12 = self.__dict2query("node_children12", node_children12)
        node_ch21 = self.__dict2query("node_children21", node_children21)

        query = "MATCH (" + node_ch11 + ")<--(" + node + ")-->(" + node_ch12 + ")-->(" + node_ch21 + ") RETURN node, node_children12, node_children21";
        # ~ print (query)
        session = self.graph_connection.session()
        result = session.run(query)
        session.closed()

        rlist2 = self.__neoj2graph_query2nodes2(result)
        return rlist2

    def MatchBranch(self, node, name_branch):
        node = self.__dict2query("node", node)
        # node_ch11 = self.__dict2query("r", name_branch)

        query = "MATCH (" + node + ")-[:" + name_branch + "]->(m) RETURN m";
        # print (query)
        session = self.graph_connection.session()
        result = session.run(query)
        session.closed()

        rlist2 = self.__neoj2graph_query2nodes2(result)
        return rlist2

    def MatchChain(self, chains):
        li = []
        i = 0
        res_nod = "(%s)" % (self.__dict2query("node", chains[-1]))
        # ~ print (type(res_nod),res_nod)
        for ch in chains[:-1]:
            tname = "ch%d" % (i)
            descr_node = "(%s)" % (self.__dict2query(tname, ch))
            li.append(descr_node)
            i += 1
        query = "MATCH %s --> %s return node" % (" --> ".join(li), res_nod)
        # print (query)
        session = self.graph_connection.session()
        result = session.run(query)
        session.closed()
        rlist2 = self.__neoj2graph_query2nodes(result)
        return rlist2

    def SimpleQuest(self, quest, params):
        params_str = tuple(["{" + self.__format_args(dic) + '}' for dic in params])
        query = quest % params_str
        # print(query)
        session = self.graph_connection.session()
        result = session.run(query)
        rlist2 = self.__neoj2graph_query2nodes(result)
        session.closed()
        return rlist2

    def ConnectsTo(self, node):
        if self.graph_segment is not None:
            if type(node) is dict:
                node["_segment"] = self.graph_segment
            else:
                node.dict["_segment"] = self.graph_segment

        '''finds all nodes in the graph that are connected to selected node'''
        # node1 = self.__dict2query("n", node)
        if "id" in node:
            query = "START n = NODE(" + str(node["id"]) + ')' + "MATCH (" + "n" + ")-[r]->(node) RETURN node"
            # print (query)
        else:
            node1 = self.__dict2query("n", node)
            query = "match (" + node1 + ")-[r]->(node) return (node)"
            # print (query)
        session = self.graph_connection.session()
        result = session.run(query)
        rlist2 = self.__neoj2graph_query2nodes(result)
        session.closed()
        return rlist2

    def ConnectsToLabel(self, node):
        if self.graph_segment is not None:
            if type(node) is dict:
                node["_segment"] = self.graph_segment
            else:
                node.dict["_segment"] = self.graph_segment

        '''finds all nodes in the graph that are connected to selected node'''
        # node1 = self.__dict2query("n", node)
        if "id" in node:
            query = "START n = NODE(" + str(node["id"]) + ')' + "MATCH (" + "n" + ")-[r]->(node) RETURN type(r)"
            #print (query)
        else:
            node1 = self.__dict2query("n", node)
            query = "match (" + node1 + ")-[r]->(node) return type(r)"
        session = self.graph_connection.session()
        result = session.run(query)
        ls_res = []
        for r in result:
            ls_res.append(r.values()[0])
        #rlist2 = self.__neoj2graph_query2nodes2(result)
        session.closed()
        return ls_res

    def ConnectedWith(self, node):
        if self.graph_segment is not None:
            if type(node) is dict:
                node["_segment"] = self.graph_segment
            else:
                node.dict["_segment"] = self.graph_segment

        '''finds all nodes in the graph that are connected to selected node'''
        # node1 = self.__dict2query("n", node)
        if "id" in node:
            query = "START n = NODE(" + str(node["id"]) + ')' + "MATCH (" + "n" + ")<-[r]-(node) RETURN node"
            # print (query)
        else:
            node1 = self.__dict2query("n", node)
            query = "match (" + node1 + ")<-[r]-(node) return (node)"
        # print (query)
        session = self.graph_connection.session()
        result = session.run(query)
        rlist2 = self.__neoj2graph_query2nodes(result)
        session.closed()
        return rlist2

    def DeleteNode(self, node):
        '''Remove node from graph'''
        if self.graph_segment is not None:
            if type(node) is dict:
                node["_segment"] = self.graph_segment
            else:
                node.dict["_segment"] = self.graph_segment

        node1 = self.__dict2query("n", node)
        # ~ print ("DeleteNode", node1)
        if 'type' in node and node['type'] == 'пользователь':
            query = "MATCH (n{_segment:'" + str(node['_segment']) + "',создано:'" + str(
                node['создано']) + "',номер:'" + str(node['номер']) + "'}),(m:_user{name:'" + str(
                node['login']) + "'}) DETACH DELETE  n,m"
        else:
            if "id" in node:
                query = "START n = NODE(" + str(node["id"]) + ')' + "MATCH (" + node1 + ") DETACH DELETE n"
            else:
                query = "MATCH (" + node1 + ") DETACH DELETE n"
        print(query)
        session = self.graph_connection.session()
        session.run(query)
        # rlist2 = self.__neoj2graph_query2nodes(result)
        try:
            session.closed()
        except:
            pass
        session.closed()

    def DeleteProp(self, node, prop):
        '''Remove node from graph'''
        if self.graph_segment is not None:
            if type(node) is dict:
                node["_segment"] = self.graph_segment
            else:
                node.dict["_segment"] = self.graph_segment

        node1 = self.__dict2query("n", node)
        if "id" in node:
            query = "START n = NODE(" + str(node["id"]) + ')' + "MATCH (" + node1 + ") REMOVE n." + prop + " RETURN n"
        else:
            query = "MATCH (" + node1 + ") REMOVE n." + prop + " RETURN n"
        session = self.graph_connection.session()
        session.run(query)
        session.closed()

    def SetEdge(self, node, node_ch, new_edge):
        query = "MATCH (node1)-[rel]->(node2) WHERE id(node1) = " + str(node['id']) + " and id(node2)=" + str(
            node_ch['id']) + " DELETE rel"
        session = self.graph_connection.session()
        result = session.run(query)
        session.closed()
        rlist2 = self.__neoj2graph_query2nodes2(result)
        query = "MATCH (node1),(node2) WHERE id(node1) = " + node['id'] + " and id(node2)=" + str(
            node_ch['id']) + " CREATE UNIQUE (node1)-[conn:" + '|'.join(new_edge) + "]->(node2)"
        session = self.graph_connection.session()
        result = session.run(query)
        session.closed()
        rlist2 = self.__neoj2graph_query2nodes2(result)
        return rlist2

    def DetachNode(self, parent, child):
        '''Remove node from graph'''
        if self.graph_segment is not None:
            if type(parent) is dict:
                parent["_segment"] = self.graph_segment
            else:
                parent.dict["_segment"] = self.graph_segment
            if type(child) is dict:
                child["_segment"] = self.graph_segment
            else:
                child.dict["_segment"] = self.graph_segment

        if not "id" in child:
            try:
                child = parent.child(child)
                print("child node found")
            except:
                print("no children found for node, exiting")
                return

        if not "id" in parent:
            try:
                parent = child.parents(parent)
                print("parent node found")
            except:
                print("no parent found for node, exiting")
                return

        if type(parent) is basics.NodeList:
            for p in parent:
                query = "MATCH  (node1)-[rel]->(node2) WHERE  id(node1) = " + p['id'] + " and id(node2)=" + str(
                    child['id']) + " DELETE rel"
                session = self.graph_connection.session()
                print(query)
                session.run(query)
                session.closed()

        else:
            query = "MATCH  (node1)-[rel]->(node2) WHERE  id(node1) = " + parent['id'] + " and id(node2)=" + str(
                child['id']) + " DELETE rel"
            print(query)

            session = self.graph_connection.session()
            session.run(query)
            # rlist2 = self.__neoj2graph_query2nodes(result)
            session.closed()

    def UpdateNodeProperty(self, node, property_name, new_property_value):
        '''Updates value of node property at the database for
           permanent storage'''
        if self.graph_segment is not None:
            node.dict["_segment"] = self.graph_segment

        node1 = self.__dict2query("n", node)
        new_property_value = str(new_property_value)
        new_property_value = new_property_value.replace("'", '"')
        new_property_value = new_property_value.replace('"', '\"')

        if "id" in node:
            query = "MATCH (node) WHERE id(node)= " + str(
                node["id"]) + " SET node." + property_name + " = '" + new_property_value + "' RETURN node"

        else:

            query = "MATCH (" + node1 + ") SET n." + property_name + " = '" + new_property_value + "' RETURN n"

        print(query)
        session = self.graph_connection.session()
        session.run(query)
        session.close()

    def TopLevelSections(self, node):
        nodes = self.Match(node).First()
        session = self.graph_connection.session()
        query = "MATCH (node:`раздел`{_segment:'" + nodes[
            '_segment'] + "'}) where not (node)<-[:CONNECTS]-(:раздел) RETURN node"
        print(query)
        result = session.run(query)
        session.closed()
        rlist2 = self.__neoj2graph_query2nodes(result)
        return rlist2

    def SetRemove(self, node, type_new):
        if type(node) is basics.NodeList:
            nodes = self.Match(node[0]).First()
        else:
            nodes = self.Match(node).First()
        session = self.graph_connection.session()
        query = "MATCH (node) WHERE ID(node)=" + nodes['id'] + " SET node:" + type_new + " REMOVE node:" + nodes[
            'type'] + " RETURN node"
        print(query)
        result = session.run(query)
        session.closed()
        rlist2 = self.__neoj2graph_query2nodes(result)
        return rlist2

    def DeleteMatchNotice(self, user):
        nodes = self.Match(user).First()
        nodes_ch = self.ConnectsTo(nodes)
        list_notice = []
        for ch in nodes_ch:
            if ch['type'] == 'уведомление':
                if 'прочитано' in ch and ch['прочитано'] == 'да':
                    self.DeleteNode(ch)
                else:
                    self.UpdateNodeProperty(ch, 'прочитано', 'да')
                    list_notice.append(ch)
        return sorted(list_notice, key=lambda x: (-float(x['создано'])))

    def MatchHou(self, node, user):
        if 'type' in node and node['type'] == 'уведомление':
            res = self.DeleteMatchNotice(user)
        else:
            res = self.Match(node)
        return res

    def Match2Chaild1Child2(self, node_parent, node_children12, node_children21):
        """
            node-->ch12-->ch21
            return node
        """
        node = self.__dict2query("node", node_parent)
        node_ch12 = self.__dict2query("node_children12", node_children12)
        node_ch21 = self.__dict2query("node_children21", node_children21)

        query = "MATCH (" + node + ")-->(" + node_ch12 + ")-->(" + node_ch21 + ") RETURN node_children21";
        print(query)
        session = self.graph_connection.session()
        result = session.run(query)
        session.closed()

        rlist2 = self.__neoj2graph_query2nodes2(result)
        return rlist2

    def Match2Chaild1Id(self, node_parent, node_children12, node_children21):
        """
            node-->ch12-->ch21
            return node
        """
        node = self.__dict2query("node", node_parent)
        node_ch12 = self.__dict2query("node_children12", node_children12)
        node_ch21 = self.__dict2query("node_children21", node_children21)

        query = "MATCH (" + node + ")-->(" + node_ch12 + ")-->(" + node_ch21 + ") WHERE node_children21.раздел ENDS WITH 'name_2' or node_children21.раздел ENDS WITH 'name_1' or node_children21.раздел ENDS WITH 'date' RETURN node_children21";
        print(query)
        session = self.graph_connection.session()
        result = session.run(query)
        session.closed()

        rlist = self.__neoj2graph_query2nodes2(result)
        return rlist

    def deleteChildNode(self, node):
        node1 = self.__dict2query("n", node)
        if "id" in node:
            query = "MATCH (g)-[j]->(n)-[r]->(m)-[t]->(y) WHERE id(n)= " + str(node["id"]) + ' delete n,m,y,r,t,j'
        else:
            query = "MATCH (g)-[j]->(" + node1 + ")-[r]->(m)-[t]->(y) delete n,m,y,r,t,j"
        print(query)
        try:
            session = self.graph_connection.session()
            session.run(query)
            session.close()
        except:
            self.deleteChildNode2(node)

    def deleteChildNode2(self, node):
        node1 = self.__dict2query("n", node)
        if "id" in node:
            query = "MATCH (n)-->(y) WHERE id(n)= " + str(node["id"]) + ' detach delete y detach delete n'
        else:
            query = "MATCH (" + node1 + ")-->(y) detach delete y detach delete n"
        print(query)
        session = self.graph_connection.session()
        s = session.run(query)
        print(s)
        print(self.__neoj2graph_query2nodes2(s))
        session.close()

    def Matchgrandchild(self, node_parent, node_children11, node_children12, node_children21):
        """
            ch11<--node-->ch12-->ch21
            return node
        """
        node = self.__dict2query("node", node_parent)
        node_ch11 = self.__dict2query("node_children11", node_children11)
        node_ch12 = self.__dict2query("node_children12", node_children12)
        node_ch21 = self.__dict2query("node_children21", node_children21)
        # node_ch22 = self.__dict2query("node_children22", node_children22)

        query = "MATCH (" + node + ")<--(t)<--(" + node_ch11 + ")-->(u)-->(" + node_ch12 + "),(node_children11)-->(u1)-->(" + node_ch21 + ")where node.номер_строки = node_children21.номер_строки and node.номер_строки = node_children12.номер_строки RETURN node_children12,node_children21";
        print(query)
        session = self.graph_connection.session()
        result = session.run(query)
        session.closed()

        rlist2 = self.__neoj2graph_query2nodes2(result)
        return rlist2

    def DeleteEdge(self, id_edge):
        session = self.graph_connection.session()
        query = "MATCH (n)-[r]->(m) WHERE ID(r)=" + id_edge + " DELETE r"
        print(query)
        result = session.run(query)
        session.closed()
        rlist2 = self.__neoj2graph_query2nodes(result)
        return rlist2
